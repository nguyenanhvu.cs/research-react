import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'
import {FormControl, FormLabel, FormGroup} from 'material-ui/Form'
import {withStyles} from "material-ui";
import Grid from "material-ui/es/Grid/Grid";
import Paper from "material-ui/es/Paper/Paper";
import isEmail from 'validator/lib/isEmail'

const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
    root: {
        flexGrow: 1,
        marginTop: 30,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        height: '100%',
    },
    control: {
        padding: theme.spacing.unit,
    }
});

class FormRegister extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            password: '',
            emailValid: false,
            passwordValid: false,
            errorMessageEmail: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
        if(name === 'email') {
            this.handleEmail(this.state.email)
        }
    };

    handleSubmit(e) {
        alert("submit form" + this.state.firstName + " " + this.state.lastName + " " + this.state.email+ " " + this.state.phoneNumber);
        e.preventDefault()
    };

    handleEmail = (value: string): void => {
        if (!isEmail(value, ['require_display_name'])) {
            this.setState({'emailValid': true, 'errorMessageEmail': 'Email invalid'});
        } else {
            this.setState({'emailValid': false, 'errorMessageEmail': ''});
        }
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={24} justify="center">
                    <Grid item md={3}>
                        <Paper className={classes.paper}>
                            <Grid item md={12}>
                                <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                                    <FormControl fullWidth>
                                        <FormLabel component="legend">Register</FormLabel>
                                        <FormGroup>
                                            <TextField
                                                required
                                                id="firstName"
                                                label="First Name"
                                                className={classes.TextField}
                                                value={this.state.firstName}
                                                onChange={this.handleChange('firstName')}
                                            />

                                            <TextField
                                                id="lastName"
                                                label="Last Name"
                                                className={classes.TextField}
                                                value={this.state.lastName}
                                                onChange={this.handleChange('lastName')}
                                            />
                                            <TextField
                                                id="email"
                                                label="Email"
                                                className={classes.TextField}
                                                value={this.state.email}
                                                onChange={this.handleChange('email')}
                                                error={this.state.emailValid}
                                                helperText={this.state.errorMessageEmail}
                                            />
                                            <TextField
                                                id="phoneNumber"
                                                label="Phone"
                                                className={classes.TextField}
                                                value={this.state.phoneNumber}
                                                onChange={this.handleChange('phoneNumber')}
                                            />

                                            <TextField
                                                id="password"
                                                label="Password"
                                                className={classes.TextField}
                                                value={this.state.password}
                                                onChange={this.handleChange('password')}
                                            />
                                            <Button raised color="primary" className={classes.button} type="submit">
                                                Register
                                            </Button>
                                        </FormGroup>
                                    </FormControl>
                                </form>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </div>

        );
    }
}

FormRegister.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormRegister);