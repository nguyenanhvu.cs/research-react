import * as React from "react";
import axios from 'axios'
import host from '../resources/config'



class Account extends React.Component {
    constructor(props) {
        super(props)
    }
    async getAccounts() {
        try {
            const responsive = await axios.get(`${host}/tobtc?currency=USD&value=500`);
            const data = responsive.data;
            console.log(data);
            return data;
        } catch (error) {
            console.log(error);
        }
    };
}